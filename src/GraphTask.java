import sun.security.provider.certpath.Vertex;

import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     *
     * The ceated algorithm is a modified DFS algorithm that asks for two vertices in a graph and finds
     * all the existing paths between them. To use the algorithm getpaths(int origin, int target)
     * method must be used on a graph. In the run() method below are five tests that are more deeply explained in the
     * homework report and console printouts when the main method is run.
     */
    public void run() {


        Graph g = new Graph("G");

        try {
            g.TestEmptyGraph();
        } catch (Exception e) {
            System.out.println(e);
        }

        g.TestSimpleGraph();

        try {
            g.TestOutOfBoundsOrigin();
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            g.TestOutOfBoundsTarget();
        } catch (Exception e) {
            System.out.println(e);
        }

        Graph s = new Graph("s");

        s.Backtracking();


        // TODO!!! Your experiments here
    }

    // TODO!!! add javadoc relevant to your problem
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public List<Vertex> getAdjVertices() {
            List<Vertex> adjlist = new ArrayList<>();
            Arc addedarc = first;

            while (addedarc != null) {
                adjlist.add(addedarc.target);
                addedarc = addedarc.next;
            }

            return adjlist;
        }


        // TODO!!! Your Vertex methods here!
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        // You can add more fields, if needed

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
        // TODO!!! Your Arc methods here!
    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        public void calculateArcs(Vertex t, LinkedList<Vertex> blacklist) {

            //Adjacency list of vertices is created
            List<Vertex> surrvert = blacklist.getLast().getAdjVertices();


            for (Vertex vertinlist : surrvert) {
                if (blacklist.contains(vertinlist)) {
                    continue;
                }

                //Target vertex is found and path is printed out
                if (vertinlist.equals(t)) {
                    blacklist.add(vertinlist);
                    printPath(blacklist);
                    blacklist.removeLast();
                    break;
                }
            }

            for (Vertex vertinlist : surrvert) {

                if (blacklist.contains(vertinlist) || vertinlist.equals(t)) {
                    continue;
                }

                blacklist.addLast(vertinlist);
                calculateArcs(t, blacklist);
                blacklist.removeLast();
            }


        }

        //Method to print out and display paths
        private void printPath(LinkedList<Vertex> blacklist) {
            for (Vertex vert : blacklist) {
                System.out.print(vert);
                System.out.print(" ");
            }
            System.out.println();
        }

        //Method for the user to input the origin and target vertices as well as error checking
        public void getpaths(int origin, int target) {

            LinkedList<Vertex> blacklist = new LinkedList<>();
            LinkedList<Vertex> verts = new LinkedList<>();

            Vertex firstver = first;
            while (firstver != null) {
                verts.add(firstver);
                firstver = firstver.next;
            }

            if (verts.isEmpty()) {
                throw new RuntimeException("The graph " + id + " is empty.");
            }

            try {
                verts.get(origin - 1);
            } catch (Exception e) {
                throw new RuntimeException("The entered origin vertex v" + origin + " does not exist in this graph." +
                        " Available Vertices are: " + verts+". Please enter a number between " +
                        ""+(verts.indexOf(verts.getFirst())+1)+" and "+(verts.indexOf(verts.getLast())+1)+".");
            }

            try {
                verts.get(target - 1);
            } catch (Exception e) {
                throw new RuntimeException("The entered target vertex v" + target + " does not exist in this graph." +
                        " Available Vertices are: " + verts+". Please enter a number between " +
                        ""+(verts.indexOf(verts.getFirst())+1)+" and "+(verts.indexOf(verts.getLast())+1)+".");
            }

            if (origin == target) {
                throw new RuntimeException("The entered origin and target vertices are the same." +
                        "Please check your inputs.");
            }

            Vertex originvert = verts.get(origin - 1);
            Vertex targetvert = verts.get(target - 1);

            blacklist.add(originvert);
            calculateArcs(targetvert, blacklist);

        }

        //Tests that are further explained in the text homework report and console print outs
        void TestSimpleGraph() {

            System.out.println(" ");
            System.out.println("Test to print out paths on a simple graph");
            Graph simpgraph = this;
            simpgraph.createRandomSimpleGraph(8, 10);
            simpgraph.toString();
            System.out.println(simpgraph);
            System.out.println(" ");
            simpgraph.getpaths(1, 8);
            System.out.println(" ");
        }

        void TestOutOfBoundsOrigin() {
            System.out.println(" ");
            System.out.println("Test to input an origin vertex that doesn't exist");
            Graph simpgraph = this;
            simpgraph.createRandomSimpleGraph(8, 10);
            simpgraph.getpaths(0, 8);
        }

        void TestOutOfBoundsTarget() {
            System.out.println(" ");
            System.out.println("Test to input a target vertex that doesn't exist");
            Graph simpgraph = this;
            simpgraph.createRandomSimpleGraph(8, 10);
            simpgraph.getpaths(1, 10);
        }

        void TestEmptyGraph() {
            System.out.println(" ");
            System.out.println("Test to find paths in an empty graph");
            Graph simpgraph = this;
            simpgraph.getpaths(1, 10);

        }

        void Backtracking() {
            System.out.println(" ");
            System.out.println("Backtracking test. Test to see if the method can backtrack from the path " +
                    "v4->v3->v2 and print out the correct path v1 v2 v5 v6.");
            Vertex v1 = this.createVertex("v1");
            Vertex v2 = this.createVertex("v2");
            Vertex v3 = this.createVertex("v3");
            Vertex v4 = this.createVertex("v4");
            Vertex v5 = this.createVertex("v5");
            Vertex v6 = this.createVertex("v6");
            Arc v1_v2 = this.createArc("v1_v2", v1, v2);
            Arc v2_v1 = this.createArc("v2_v1", v2, v1);
            Arc v2_v3 = this.createArc("v2_v3", v2, v3);
            Arc v3_v2 = this.createArc("v3_v2", v3, v2);
            Arc v3_v4 = this.createArc("v3_v4", v3, v4);
            Arc v4_v3 = this.createArc("v4_v3", v4, v3);
            Arc v2_v5 = this.createArc("v2_v5", v2, v5);
            Arc v5_v2 = this.createArc("v5_v2", v5, v2);
            Arc v5_v6 = this.createArc("v5_v6", v5, v6);
            Arc v6_v5 = this.createArc("v6_v5", v6, v5);
            this.first = v1;
            v1.next = v2;
            v2.next = v3;
            v3.next = v4;
            v4.next = v5;
            v5.next = v6;
            v6.next = null;
            v1.first = v1_v2;
            v2.first = v2_v1;
            v2.first = v2_v3;
            v2.first = v2_v5;
            v3.first = v3_v2;
            v3.first = v3_v4;
            v4.first = v4_v3;
            v5.first = v5_v2;
            v5.first = v5_v6;
            v6.first = v6_v5;
            this.toString();
            System.out.println(this);
            getpaths(1, 6);
        }


        // TODO!!! Your Graph methods here! Probably your solution belongs here.
    }

}


